% -*- latex -*-

\chapter{Filter Type Reference}
\label{chap:FilterTypeReference}

\index{filter!implementation|(}

In Chapters \ref{chap:SimpleWorklets} and \ref{chap:WorkletTypeReference} we discuss how to implement an algorithm in the \VTKm framework by creating a worklet.
Worklets might be straightforward to implement and invoke for those well familiar with the appropriate \VTKm API.
However, novice users have difficulty using worklets directly.
For simplicity, worklet algorithms are generally wrapped in what are called filter objects for general usage.
Chapter \ref{chap:RunningFilters} introduces the concept of filters and documents those that come with the \VTKm library.
Chapter \ref{chap:BasicFilterImpl} gave a brief introduction on implementing filters.
This chapter elaborates on building new filter objects by introducing new filter types.
These will be used to wrap filters around the extended worklet examples in Chapter \ref{chap:WorkletTypeReference}.

Unsurprisingly, the base filter objects are contained in the \vtkmfilter{} package.
The basic implementation of a filter involves subclassing one of the base filter objects and implementing the \classmember*{Filter}{DoExecute} method.
The \classmember*{Filter}{DoExecute} method performs the operation of the filter and returns a new data set containing the result.

As with worklets, there are several flavors of filter types to address different operating behaviors although their is not a one-to-one relationship between worklet and filter types.
This chapter is sectioned by the different filter types with an example of implementations for each.


\section{Field Filters}
\label{sec:CreatingFieldFilters}

\index{filter!field|(}
\index{field filter|(}

Field filters are a category of filters that generate a new field.
These new fields are typically derived from one or more existing fields or point coordinates on the data set.
For example, mass, volume, and density are interrelated, and any one can be derived from the other two.

Field filters are implemented in classes that derive the \vtkmfilter{FilterField} base class.
\textidentifier{FilterField} is a templated class that has a single template argument, which is the type of the concrete subclass.
The propagation of additional fields from the input to the output is handled automatically by \vtkmfilter{FilterField}. If the default
of automatic propagation isn't desired the caller can select which fields to propagate via \classmember*{Filter}{SetFieldsToPass}.


\begin{didyouknow}
  The convention of having a subclass be templated on the derived class' type is known as the Curiously Recurring Template Pattern (CRTP).
  In the case of \textidentifier{FilterField} and other filter base classes, \VTKm uses this CRTP behavior to allow the general implementation of these algorithms to run \classmember*{Filter}{DoExecute} in the subclass, which as we see in a moment is itself templated.
\end{didyouknow}

All \textidentifier{FilterField} subclasses must implement a \classmember*{Filter}{DoExecute} method.
The \textidentifier{FilterField} base class implements an \classmember*{FilterField}{Execute} method (actually several overloaded versions of \classmember*{FilterField}{Execute}), processes the arguments, and then calls the \classmember*{Filter}{DoExecute} method of its subclass.
The \classmember*{Filter}{DoExecute} method has the following 4 arguments.
\begin{itemize}
\item
  An input data set contained in a \vtkmcont{DataSet} object.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  The field from the \textidentifier{DataSet} specified in the \classmember*{FilterField}{Execute} method to operate on.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:BasicArrayHandles} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  See Section \ref{sec:ApplyingPolicies} for information on using policies.
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

In this section we provide an example implementation of a field filter that wraps the ``magnitude'' worklet provided in Example~\ref{ex:UseWorkletMapField} (listed on page~\pageref{ex:UseWorkletMapField}).
By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{FieldMagnitude.h}.

\vtkmlisting[ex:MagnitudeFilterDeclaration]{Header declaration for a field filter.}{UseFilterField.cxx}

\index{filter!supported types|(}

Notice that within the declaration of \textcode{FieldMagnitude} in Example~\ref{ex:MagnitudeFilterDeclaration}, there is also the declaration of a type named \supportedtypes (starting on line \ref{ex:UseFilterField.cxx:SupportedTypes}).
This type provides a type list containing all the types that are valid for the input field.
(Type lists are discussed in detail in Section~\ref{sec:TypeLists}.)
In the particular case of our \textcode{FieldMagnitude} filter, the filter expects to operate on some type of vector field.
Thus, \supportedtypes is set to a list of all standard floating point \textidentifier{Vec}s.

\index{filter!supported types|)}

Once the filter class is declared in the \textfilename{.h} file, the implementation filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{FieldMagnitude.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

The implementation of \classmember*{Filter}{DoExecute} is straightforward.
A worklet is invoked to compute a new field array.
\classmember*{Filter}{DoExecute} then returns a newly constructed \vtkmcont{DataSet} object containing the result.
There are numerous ways to create a \textidentifier{DataSet}, but to simplify making filters, \VTKm provides the \vtkmfilter{CreateResult} function to simplify the process.
There are several overloaded versions of \vtkmfilter{CreateResult} (defined in header file \vtkmheader{vtkm/filter}{CreateResult.h}), but the simplest one to use for a filter that adds a field takes the following 5 arguments.
\begin{itemize}
\item
  The input data set.
  This is the same data set passed to the first argument of \classmember*{Filter}{DoExecute}.
\item
  The array containing the data for the new field, which was presumably computed by the filter.
\item
  The name of the new field.
\item
  A \vtkmfilter{FieldMetadata} object containing information about the association of the array to which topological elements of the input (for example cells or points).
  Typically, this \textidentifier{FieldMetadata} object comes from the input parameters of \classmember*{Filter}{DoExecute}.
\end{itemize}

Note that this form of \textidentifier{CreateResult} is generally applicable when the field created is of the same type as the input field.
For example, when computing the magnitude of a vector field, if the input vectors are on the points, the output scalars will also be on the points and likewise if the input is on the cells.
In the case where the field association changes, it is better to use a different form of \textidentifier{CreateResult}.
If the output field is associated with points, such as when averaging cell fields to each point, then it is best to use \vtkmfilter{CreateResultFieldPoint}.
Likewise, if the output field is associated with cells, such as when computing the volume of each cell, then it is best to use \vtkmfilter{CreateResultFieldCell}.
Both functions only need the three arguments of the input data set, the array for the field's data, and the name of the new field.

Note that all fields need a unique name, which is the reason for the third argument to \textidentifier{CreateResult}.
The \vtkmfilter{FilterField} base class contains a pair of methods named \classmember*{FilterField}{SetOutputFieldName} and \classmember*{FilterField}{GetOutputFieldName} to allow users to specify the name of output fields.
The \classmember*{Filter}{DoExecute} method should respect the given output field name.
However, it is also good practice for the filter to have a default name if none is given.
This might be simply specifying a name in the constructor, but it is worthwhile for many filters to derive a name based on the name of the input field.

\vtkmlisting{Implementation of a field filter.}{FilterFieldImpl.cxx}

\subsection{Using Cell Connectivity}
\label{sec:CreatingFieldFiltersWithCellConnectivity}

\index{filter!field!using cells|(}
\index{field filter!using cells|(}

The previous example performed a simple operation on each element of a field independently.
However, it is also common for a ``field'' filter to take into account the topology of a data set.
The interface for the filter is similar, but the implementation involves pulling a \vtkmcont{CellSet} from the input \vtkmcont{DataSet} and performing operations on fields associated with different topological elements.
The steps involve calling \classmember{DataSet}{GetCellSet} to retrieve the \textidentifier{CellSet} object, applying the policy on that object, and then using topology-based worklets, described in Section \ref{sec:TopologyMaps}, to operate on them.

In this section we provide an example implementation of a field filter on cells that wraps the ``cell center'' worklet provided in Example~\ref{ex:UseWorkletVisitCellsWithPoints} (listed on page~\pageref{ex:UseWorkletVisitCellsWithPoints}).
By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{CellCenter.h}.

\vtkmlisting[ex:CellCenterFilterDeclaration]{Header declaration for a field filter using cell topology.}{UseFilterFieldWithCells.cxx}

\begin{didyouknow}
  \index{filter!supported types} You may have noticed that Example~\ref{ex:MagnitudeFilterDeclaration} provided a specification for \supportedtypes but Example~\ref{ex:CellCenterFilterDeclaration} provides no such specification.
  This demonstrates that declaring \supportedtypes is optional.
  If a filter only works on some limited number of types, then it can use \supportedtypes to specify the specific types it supports.
  But if a filter is generally applicable to many field types, it can simply use the default filter types.
\end{didyouknow}

Once the filter class is declared in the \textfilename{.h} file, the implementation filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{CellCenter.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

As with any subclass of \textidentifier{FilterField}, the filter implements \classmember*{FilterField}{DoExecute}, which in this case invokes a worklet to compute a new field array and then return a newly constructed \vtkmcont{DataSet} object.

\vtkmlisting[ex:CellCenterFilterImplementation]{Implementation of a field filter using cell topology.}{FilterFieldWithCellsImpl.cxx}

\begin{commonerrors}
  The policy passed to the \classmember*{Filter}{DoExecute} method contains information on what types of cell sets should be supported by the execution.
  This list of cell set types could be different than the default types specified the \textidentifier{DynamicCellSet} returned from \classmember*{DataSet}{GetCellSet}.
  \index{policy} Thus, it is important to apply the policy to the cell set before passing it to the \classmember*{Filter}{Invoke} method.
  The policy is applied by calling the \vtkmfilter{ApplyPolicy} function on the \textidentifier{DynamicCellSet}.
  The use of \textidentifier{ApplyPolicy} is demonstrated in Example~\ref{ex:CellCenterFilterImplementation}.
  \fix{The details of this may change when moving to virtual methods.}
\end{commonerrors}

\index{field filter!using cells|)}
\index{filter!field!using cells|)}

\index{field filter|)}
\index{filter!field|)}

\section{Data Set Filters}
\label{sec:CreatingDataSetFilters}

\index{filter!data set|(}
\index{data set filter|(}

Data set filters are a category of filters that generate a data set with a new cell set based off the cells of an input data set.
For example, a data set can be significantly altered by adding, removing, or replacing cells.

Data set filters are implemented in classes that derive the \vtkmfilter{FilterDataSet} base class.
\textidentifier{FilterDataSet} is a templated class that has a single template argument, which is the type of the concrete subclass.

All \textidentifier{FilterDataSet} subclasses must implement two methods: \classmember*{FilterDataSet}{DoExecute} and \classmember*{FilterDataSet}{DoMapField}.
The \textidentifier{FilterDataSet} base class implements \classmember*{FilterDataSet}{Execute} and \classmember*{FilterDataSet}{MapFieldOntoOutput} methods that process the arguments and then call the \classmember*{FilterDataSet}{DoExecute} and \classmember*{FilterDataSet}{DoMapField} methods, respectively, of its subclass.

Like \vtkmfilter{FilterField} all fields are considered to be eligible for propagation. If this behavior is not desired by the caller
they can select which fields to propagate via \classmember*{Filter}{SetFieldsToPass}. The actual transformation of the fields so they
are valid is handled by \classmember*{FilterDataSet}{DoMapField}.

The \classmember*{FilterDataSet}{DoExecute} method has the following 2 arguments.
\begin{itemize}
\item
  An input data set contained in a \vtkmcont{DataSet} object.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  A policy class.
  See Section \ref{sec:ApplyingPolicies} for information on using policies.
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

The \classmember*{FilterDataSet}{DoMapField} method has the following 4 arguments.
\begin{itemize}
\item
  A \vtkmcont{DataSet} object that was returned from the last call to \classmember*{FilterDataSet}{DoExecute}.
  The method both needs information in the \textidentifier{DataSet} object and writes its results back to it, so the parameter should be declared as a non-const reference.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  A field from the \textidentifier{DataSet} specified in the \classmember*{FilterDataSet}{Execute} method to convert so it is applicable to
  the output \textidentifier{DataSet}.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:BasicArrayHandles} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  See Section \ref{sec:ApplyingPolicies} for information on using policies.
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

\begin{didyouknow}
  Some \textidentifier{FilterDataSet} subclasses have no need to process each individual field as they can safely propagate or drop
  all fields. In these cases it is better to override \classmember*{FilterDataSet}{MapFieldOntoOutput} allowing you to perform this
  logic directly with \textidentifier{Field} objects and not incur the compile and runtime cost of deducing the underlying \textidentifier{ArrayHandle}.
\end{didyouknow}

In this section we provide an example implementation of a data set filter that wraps the functionality of extracting the edges from a data set as line elements.
Many variations of implementing this functionality are given in Chapter~\ref{chap:GeneratingCellSets}.
For the sake of argument, we are assuming that all the worklets required to implement edge extraction are wrapped up in structure named \vtkmworklet{}\textcode{::ExtractEdges}.
Furthermore, we assume that \textcode{ExtractEdges} has a pair of methods, \textcode{Run} and \textcode{ProcessCellField}, that create a cell set of lines from the edges in another cell set and average a cell field from input to output, respectively.
The \textcode{ExtractEdges} may hold state.
All of these assumptions are consistent with the examples in Chapter~\ref{chap:GeneratingCellSets}.

By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{ExtractEdges.h}.

\vtkmlisting{Header declaration for a data set filter.}{ExtractEdgesFilterDeclaration.cxx}

Once the filter class is declared in the \textfilename{.h} file, the implementation of the filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{ExtractEdges.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

The implementation of \classmember*{FilterDataSet}{DoExecute} first calls the worklet methods to generate a new \textidentifier{CellSet} class.
It then constructs a \textidentifier{DataSet} containing this \textidentifier{CellSet}.
It also has to pass all the coordinate systems to the new \textidentifier{DataSet}.
Finally, it returns the \textidentifier{DataSet}.

\vtkmlisting[ex:ExtractEdgesFilterDoExecute]{Implementation of the \classmember*{FilterDataSet}{DoExecute} method of a data set filter.}{ExtractEdgesFilterDoExecute.cxx}

The implementation of \classmember*{FilterDataSet}{DoMapField} checks to see what elements the given field is associated with (e.g. points or cells), processes the field data as necessary, and adds the field to the \textidentifier{DataSet}.
The \vtkmfilter{FieldMetadata} passed to \classmember*{FilterDataSet}{DoMapField} provides some features to make this process easier.
\textidentifier{FieldMetadata} contains methods to query what the field is associated with such as \classmember*{FieldMetadata}{IsPointField} and \classmember{FieldMetadata}{IsCellField}.
\textidentifier{FieldMetadata} also has a method named \classmember{FieldMetadata}{AsField} that creates a new \vtkmcont{Field} object with a new data array and metadata that matches the input.

\vtkmlisting[ex:ExtractEdgesFilterDoMapField]{Implementation of the \classmember*{FilterDataSet}{DoMapField} method of a data set filter.}{ExtractEdgesFilterDoMapField.cxx}

\index{data set filter|)}
\index{filter!data set|)}

\section{Data Set with Field Filters}
\label{sec:CreatingDataSetWithFieldFilters}

\index{filter!data set with field|(}
\index{data set with field filter|(}

Data set with field filters are a category of filters that generate a data set with a new cell set based off the cells of an input data set along with the data in at least one field.
For example, a field might determine how each cell is culled, clipped, or sliced.

Data set with field filters are implemented in classes that derive the \vtkmfilter{FilterDataSetWithField} base class.
\textidentifier{FilterDataSetWithField} is a templated class that has a single template argument, which is the type of the concrete subclass.

All \textidentifier{FilterDataSetWithField} subclasses must implement two methods: \classmember*{FilterDataSetWithField}{DoExecute} and \classmember*{FilterDataSetWithField}{DoMapField}.
The \textidentifier{FilterDataSetWithField} base class implements \classmember*{FilterDataSetWithField}{Execute} and \classmember*{FilterDataSetWithField}{MapFieldOntoOutput} methods that process the arguments and then call the \classmember*{FilterDataSetWithField}{DoExecute} and \classmember*{FilterDataSetWithField}{DoMapField} methods, respectively, of its subclass.

Like \vtkmfilter{FilterField} all fields are considered to be eligible for propagation. If this behavior is not desired by the caller
they can select which fields to propagate via \classmember*{Filter}{SetFieldsToPass}. The actual transformation of the fields so they
are valid is handled by \classmember*{FilterDataSetWithField}{DoMapField}.

The \classmember*{FilterDataSetWithField}{DoExecute} method has the following 4 arguments.
(They are the same arguments for \classmember*{FilterDataSetWithField}{DoExecute} of field filters as described in Section~\ref{sec:CreatingFieldFilters}.)
\begin{itemize}
\item
  An input data set contained in a \vtkmcont{DataSet} object.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  The field from the \textidentifier{DataSet} specified in the \classmember*{FilterDataSetWithField}{Execute} method to operate on.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:BasicArrayHandles} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  See Section \ref{sec:ApplyingPolicies} for information on using policies.
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

The \classmember*{FilterDataSetWithField}{DoMapField} method has the following 4 arguments.
(They are the same arguments for \classmember*{FilterDataSetWithField}{DoExecute} of field filters as described in Section~\ref{sec:CreatingDataSetFilters}.)
\begin{itemize}
\item
  A \vtkmcont{DataSet} object that was returned from the last call to \classmember*{FilterDataSetWithField}{DoExecute}.
  The method both needs information in the \textidentifier{DataSet} object and writes its results back to it, so the parameter should be declared as a non-const reference.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  A field from the \textidentifier{DataSet} specified in the \classmember*{FilterDataSetWithField}{Execute} method to convert so it is
  applicable to the output \textidentifier{DataSet}.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:BasicArrayHandles} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  See Section \ref{sec:ApplyingPolicies} for information on using policies.
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

\begin{didyouknow}
  Some \textidentifier{FilterDataSetWithField} subclasses have no need to process each individual field as they can safely propagate or drop
  all fields. In these cases it is better to override \classmember*{FilterDataSetWithField}{MapFieldOntoOutput} allowing you to perform this
  logic directly with \textidentifier{Field} objects and not incur the compile and runtime cost of deducing the underlying \textidentifier{ArrayHandle}.
\end{didyouknow}

In this section we provide an example implementation of a data set with field filter that blanks the cells in a data set based on a field that acts as a mask (or stencil).
Any cell associated with a mask value of zero will be removed.

We leverage the worklets in the \VTKm source code that implement the \textidentifier{Threshold} functionality.
The threshold worklets are templated on a predicate class that, given a field value, returns a flag on whether to pass or cull the given cell.
The \vtkmfilter{Threshold} class uses a predicate that is true for field values within a range.
Our blank cells filter will instead use the predicate class \vtkm{NotZeroInitialized} that will cull all values where the mask is zero.

By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{BlankCells.h}.

\vtkmlisting[ex:BlankCellsFilterDeclaration]{Header declaration for a data set with field filter.}{BlankCellsFilterDeclaration.cxx}

Note that the filter class declaration contains a specification for \supportedtypes to specify that the field is meant to operate specifically on scalar types.
Once the filter class is declared in the \textfilename{.h} file, the implementation of the filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{BlankCells.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

The implementation of \classmember*{FilterDataSetWithField}{DoExecute} first calls the worklet methods to generate a new \textidentifier{CellSet} class.
It then constructs a \textidentifier{DataSet} containing this \textidentifier{CellSet}.
It also has to pass all the coordinate systems to the new \textidentifier{DataSet}.
Finally, it returns the \textidentifier{DataSet}.

\vtkmlisting[ex:BlankCellsFilterDoExecute]{Implementation of the \classmember*{FilterDataSetWithField}{DoExecute} method of a data set with field filter.}{BlankCellsFilterDoExecute.cxx}

The implementation of \classmember*{FilterDataSetWithField}{DoMapField} checks to see what elements the given field is associated with (e.g. points or cells), processes the field data as necessary, and adds the field to the \textidentifier{DataSet}.
The \vtkmfilter{FieldMetadata} passed to \classmember*{FilterDataSetWithField}{DoMapField} provides some features to make this process easier.
\textidentifier{FieldMetadata} contains methods to query what the field is associated with such as \classmember*{FieldMetaData}{IsPointField} and \classmember*{FieldMetaData}{IsCellField}.
\textidentifier{FieldMetadata} also has a method named \classmember*{FieldMetaData}{AsField} that creates a new \vtkmcont{Field} object with a new data array and metadata that matches the input.

\vtkmlisting[ex:BlankCellsFilterDoMapField]{Implementation of the \classmember*{FilterDataSetWithField}{DoMapField} method of a data set with field filter.}{BlankCellsFilterDoMapField.cxx}

\index{data set with field filter|)}
\index{filter!data set with field|)}


\section{Applying Policies}
\label{sec:ApplyingPolicies}

\index{filter!policy|(}
\index{policy|(}

As seen throughout this chapter, filters define their operation by implementing a \classmember*{Filter}{DoExecute} method.
The prototype and information passed to the method varies across the different filter types discussed later in this chapter, but one thing they all in common is that they all receive a \vtkmcont{DataSet} object that was passed to the \classmember*{Filter}{Execute} method.
Although some data extracted from the \textidentifier{DataSet} may also be passed to \classmember*{Filter}{DoExecute}, it is often necessary to gather further information from the \textidentifier{DataSet}.

The problem will retrieving data from a \textidentifier{DataSet} is that the specific types of the data are not known.
For example, you can get a \vtkmcont{Field} object from the \textidentifier{DataSet}, but to do anything of interest with the \textidentifier{Field}, you have to then pull the array out of the field.
The problem is that you do not know what type of values are stored in the array.
Nor do you know the storage mechanism for the array.

This is where \keyterm{policies} come into play.
All \classmember*{Filter}{DoExecute} methods also receive a policy object.
The policy object is really just an empty structure, but it contains important compile-time type information that can be used to deduce which types to expect when pulling fields and cell sets from a \textidentifier{DataSet}.
The policy can be applied by using one of the following functions, all of which are defined in \vtkmheader{vtkm/filter}{PolicyBase.h}.

\begin{commonerrors}
  Policies define a finite list of types to try when retrieving objects from a \textidentifier{DataSet}.
  If the \textidentifier{DataSet} holds an object of an uncommon type not checked, then a runtime error will occur.
\end{commonerrors}

\subsection{Getting Field Data}

Filter types like \textidentifier{FilterField} and \textidentifier{FilterDataSetWithField} will automatically pull an \textidentifier{ArrayHandle} from a field in the input \textidentifier{DataSet} and pass it to the subclass' \classmember*{Filter}{DoExecute}.
However, sometimes the filter will need more than one field.
Any secondary fields that the filter needs will have to be pulled from the \textidentifier{DataSet} within the implementation of the \classmember*{Filter}{DoExecute} method.

The best way to do this is to use the \vtkmfilter{ApplyPolicyFieldOfType} method.
This method takes 3 arguments: a \vtkmcont{Field} object (presumably pulled from the \textidentifier{DataSet}), the policy, and the filter (i.e. \textcode{*this}).
\textidentifier{ApplyPolicyFieldOfType} also requires an explicit template argument that specifies the value type desired.
Typically this type will be the same type as the type of the primary field's \textidentifier{ArrayHandle} provided to \classmember*{Filter}{DoExecute}.

\vtkmlisting[ex:GetSecondaryField]{Using a policy to get the \textidentifier{ArrayHandle} out of a \textidentifier{Field}.}{GetSecondaryField.cxx}

The object returned from \textidentifier{ApplyPolicyFieldOfType} is an \textidentifier{ArrayHandle}.
The value type of the \textidentifier{ArrayHandle} will be the same as the type specified in the template to \textidentifier{ApplyPolicyFieldOfType}.
So the type of \textcode{secondaryFieldArrayHandle}, defined on line \ref{ex:GetSecondaryField.cxx:secondaryFieldArrayHandle} of example \ref{ex:GetSecondaryField} is \vtkmcont{ArrayHandle}\tparams{T,...}.
The storage type of the returned \textidentifier{ArrayHandle} is rather complex and not documented here.

\begin{didyouknow}
  When using \textidentifier{ApplyPolicyFieldOfType}, the value type requested does not have to match exactly with the type in the underlying array.
  For example, if the \textidentifier{Field} contains an \textidentifier{ArrayHandle} with a value type of \vtkm{Int32} and you use \textidentifier{ApplyPolicyFieldOfType} to get an array with a value type of \vtkm{Float32}, then you will get an \textidentifier{ArrayHandle} that converts the type from \vtkm{Int32} to \vtkm{Float32}.
  This is not done with a deep copy.
  Rather, this is done any time a value is retrieved from the array (usually during worklet invocation).
\end{didyouknow}

\subsection{Getting Cell Sets}

Any filter that does operations on the topology of a \textidentifier{DataSet} needs to retrieve a \vtkmcont{CellSet} it.
Of course, \textidentifier{CellSet}s come in many varieties, and the correct one must be deduced to invoke a worklet on it.
This can be done with the \vtkmfilter{ApplyPolicyCellSet} function.

\textidentifier{ApplyPolicyCellSet} takes 2 arguments: a \vtkmcont{DynamicCellSet} (presumably pulled with \classmember{DataSet}{GetCellSet}), and the policy.

\vtkmlisting{Using a policy to get the \textidentifier{CellSet} from a \textidentifier{DataSet}.}{GetCellSet.cxx}

There are also instances where the algorithm on the \textidentifier{CellSet} is different depending on whether the \textidentifier{CellSet} is structured or unstructured (or the algorithm may only work on one or the other).
In this case you can use either \vtkmfilter{ApplyPolicyCellSetStructured} or \vtkmfilter{ApplyPolicyCellSetUnstructured} to only get \textidentifier{CellSet}s that match that type.

\vtkmlisting{Using a policy to get on structured cell sets.}{GetCellSetStructured.cxx}

\index{policy|)}
\index{filter!policy|)}

\index{filter!implementation|)}
